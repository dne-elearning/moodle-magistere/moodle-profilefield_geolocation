<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Geolocation profile field.
 *
 * @package    profilefield_geolocation
 * @copyright  2022 Reseau-Canope
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Class profile_field_geolocation
 *
 * @copyright  2022 Reseau-Canope
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class profile_field_geolocation extends profile_field_base {

    /**
     * Overwrite the base class to display the data for this field
     */
    public function display_data() {
        // Default formatting.
        return parent::display_data();
    }

    /**
     * Add fields for editing a text profile field.
     * @param moodleform $mform
     */
    public function edit_field_add($mform) {
        global $PAGE, $CFG;
        
        $longitude = $latitude = 0;
        $zoom = 1;
        $data = explode('|',$this->data);
        if (count($data) == 2){
            $longitude = $data[0];
            $latitude = $data[1];
            if ($longitude != 0 && $latitude != 0){
                $zoom = 15;
            }
        }
        
        $rtlurl = new moodle_url('/user/profile/field/geolocation/js/mapbox-gl-rtl-text.js');
        
        $PAGE->requires->jquery();
        $PAGE->requires->js('/user/profile/field/geolocation/js/leaflet.js', true);
        $PAGE->requires->js('/user/profile/field/geolocation/js/mapbox-gl.min.js', true);
        $PAGE->requires->js('/user/profile/field/geolocation/js/leaflet-mapbox-gl.min.js', true);
        $PAGE->requires->css('/user/profile/field/geolocation/css/leaflet.css');
        
        // inputname,longitude,latitude,zoom,mapurl,rtlpath
        $PAGE->requires->js_call_amd('profilefield_geolocation/map', 'init', array(
            $this->inputname,
            $longitude,
            $latitude,
            $zoom,
            $CFG->map_url,
            $rtlurl,
            get_string('saved_position','profilefield_geolocation'),
            get_string('selected_position','profilefield_geolocation')
        ));
        
        
        $html = '<div class="geolocation-map" style="height:100%;width:100%">
<div id="'.$this->inputname.'_map" style="height:300px;min-height:300px;min-width:300px;width:100%"></div>
<div class="geolocation-map-searchbar">
<input type="text" id="id_'.$this->inputname.'_searchaddress" name="'.$this->inputname.'_searchaddress" placeholder="'.get_string('search_a_location','profilefield_geolocation').'">
<button id="id_'.$this->inputname.'_searchbtn" name="'.$this->inputname.'_searchbtn">'.get_string('search','profilefield_geolocation').'</button>
</div>
</div>';
        

        // Create the form field.
        $elem = $mform->createElement('html', $html, '');
        $mform->addGroup(array($elem), 'geolocation_g1', '', array(' '), false);
        
        $group=array();
        $group[] =& $mform->createElement('static', 'longitude', 'longitude', get_string('longitude','profilefield_geolocation'));
        $group[] =& $mform->createElement('text', $this->inputname.'[longitude]', format_string($this->field->name));
        $mform->setType($this->inputname.'[longitude]', PARAM_FLOAT);
        $mform->setDefault($this->inputname.'[longitude]', $longitude);
        $group[] =& $mform->createElement('static', 'latitude', 'latitude', get_string('latitude','profilefield_geolocation'));
        $group[] =& $mform->createElement('text', $this->inputname.'[latitude]', format_string($this->field->name));
        $mform->setType($this->inputname.'[latitude]', PARAM_FLOAT);
        $mform->setDefault($this->inputname.'[latitude]', $latitude);
        
        $group[] =& $mform->createElement('button', $this->inputname.'_erase', get_string('erase','profilefield_geolocation'));
        
        $mform->addGroup($group, 'geolocation_g2', format_string($this->field->name), array(' '), false);
    }

    /**
     * Return the field type and null properties.
     * This will be used for validating the data submitted by a user.
     *
     * @return array the param type and null property
     * @since Moodle 3.2
     */
    public function get_field_properties() {
        return array(PARAM_TEXT, NULL_NOT_ALLOWED);
    }
    
    
    
    /**
     * Process incoming data for the field.
     * @param stdClass $data
     * @param stdClass $datarecord
     * @return mixed|stdClass
     */
    public function edit_save_data_preprocess($data, $datarecord) {
        if (is_array($data)) {
            $data = $data['longitude'].'|'.$data['latitude'];
        }else{
            $data = '';
        }
        return $data;
    }
}


