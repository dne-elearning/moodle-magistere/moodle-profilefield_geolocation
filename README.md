# Moodle Geolocation profile field
- Source Code: https://gitlab.com/dne-elearning/moodle-magistere/moodle-profilefield_geolocation
- License: http://www.gnu.org/licenses/gpl-3.0.html

## Install from an archive
- Extract the archive in the /user/profile/field/geolocation folder
- Install by connecting to your moodle as an administrator or user the CLI script **admin/cli/upgrade.php** if you have access to a console.
- For the background map to be displayed, you must declare the configuration variable $CFG->map_url with the link of a maptiler server style.
- (example: 'https://domain.lan/styles/osm-bright/style.json') (see https://www.maptiler.com/)

## Description

User profile field for GPS coordinate storage.

Features :
- Add a new type off user profile field which can store a GPS coordinate.
- An interactive map to easily select any location on the planet

Compatibility:
- Support any Database server supported by moodle
- The background map can only display a vector map provided by a maptiler server.