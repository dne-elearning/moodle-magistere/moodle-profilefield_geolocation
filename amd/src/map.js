define(['core/url'],function(url) {

  function init(inputname,longitude,latitude,zoom,mapurl,rtlpath,str_saved_position,str_selected_position) {
    jQuery(document).ready(function($){
	
	  if (mapboxgl.getRTLTextPluginStatus() === 'unavailable') {
        mapboxgl.setRTLTextPlugin(rtlpath);
      }
      var mymap = L.map(inputname+"_map", {"doubleClickZoom":false}).setView([latitude, longitude], zoom);
      var gl = L.mapboxGL({
        attribution: "<a href=\"http://openstreetmap.org\">OpenStreetMap</a>",
        style: mapurl
      }).addTo(mymap);
      mymap._layersMaxZoom = 20;

      function resizeMap() {
        if ($("#"+inputname+"_map").width() > 0){
          mymap.invalidateSize(true);
        }else{
          setTimeout(resizeMap, 500);
        }
      }

      resizeMap();

      var greenIcon = L.icon({
        iconUrl: url.fileUrl("/user/profile/field/geolocation/css/images", "marker-icon2.png"),
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [-3, -30],
        shadowUrl: url.fileUrl("/user/profile/field/geolocation/css/images", "marker-shadow.png"),
        shadowSize: [41, 41],
        shadowAnchor: [12.5, 41]
      });

      if (latitude != 0 && longitude != 0){
      	var markerori = L.marker([latitude, longitude]).addTo(mymap).bindPopup(str_saved_position);
      }

      var marker = L.marker([latitude, longitude], {icon:greenIcon,draggable:true}).addTo(mymap).bindPopup(str_selected_position);

      marker.on("move", function(e){
        $("#id_"+inputname+"_latitude").val(e.latlng.lat.toFixed(8));
        $("#id_"+inputname+"_longitude").val(e.latlng.lng.toFixed(8));
        marker.setPopupContent(str_selected_position);
      })

      mymap.on("dblclick", function(e){
        marker.setLatLng(e.latlng);
        marker.setPopupContent(str_selected_position);
      });

      $("button[name="+inputname+"_searchbtn]").on("click", function(e){
		e.preventDefault();
        $.get("https://nominatim.openstreetmap.org/search?format=geojson&addressdetails=0&extratags=0&namedetails=0&limit=1&q="+$("input[name="+inputname+"_searchaddress]").val(), function(data){
          if (data.features.length > 0){
            marker.setLatLng(L.GeoJSON.coordsToLatLng(data.features[0].geometry.coordinates));
            marker.setPopupContent(data.features[0].properties.display_name);
            mymap.flyToBounds([[data.features[0].bbox[1],data.features[0].bbox[0]],[data.features[0].bbox[3],data.features[0].bbox[2]]]);
          }else{
			let backcolor = $("#id_"+inputname+"_searchaddress").css( "background-color" );
            $("#id_"+inputname+"_searchaddress").animate({backgroundColor:"#ffaaaa"},500).animate({backgroundColor:backcolor},500);
          }
        })
      })

      $("button[name="+inputname+"_erase]").on("click", function(e){
		e.preventDefault();
        $("#id_"+inputname+"_latitude").val('');
        $("#id_"+inputname+"_longitude").val('');
      })

      $("#id_"+inputname+"_searchaddress").keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == "13"){
          event.preventDefault();
          $("button[name="+inputname+"_searchbtn]").click();
        }
      });

      });

    }

    return {
      init : function(inputname,longitude,latitude,zoom,mapurl,rtlpath,str_saved_position, str_selected_position) {
      init(inputname,longitude,latitude,zoom,mapurl,rtlpath,str_saved_position,str_selected_position);
    }
  };

});