<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'profilefield_geolocation', language 'fr', branch 'MOODLE_20_STABLE'
 *
 * @package   profilefield_geolocation
 * @copyright 2022 Reseau-Canope
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['erase'] = 'Effacer';
$string['latitude'] = 'Latitude';
$string['longitude'] = 'Longitude';
$string['pluginname'] = 'Géolocalisation';
$string['privacy:metadata:profilefield_text:userid'] = 'The ID of the user whose data is stored by the Text input user profile field';
$string['privacy:metadata:profilefield_text:fieldid'] = 'The ID of the profile field';
$string['privacy:metadata:profilefield_text:data'] = 'Text input user profile field user data';
$string['privacy:metadata:profilefield_text:dataformat'] = 'The format of Text input user profile field user data';
$string['privacy:metadata:profilefield_text:tableexplanation'] = 'Additional profile data';
$string['saved_position'] = 'Position enregistée';
$string['search'] = 'Chercher';
$string['search_a_location'] = 'Chercher un endroit (pays, ville, rue ...)';
$string['selected_position'] = 'Position sélectionnée';
